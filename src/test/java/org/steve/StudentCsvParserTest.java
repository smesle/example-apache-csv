package org.steve;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class StudentCsvParserTest {

    private StudentCsvParser parser;
    private List<StudentDTO> studentDTO;
    private String fileName;

    @Rule
    public ExpectedException thrown = ExpectedException.none();


    @Before
    public void setUp() throws IOException {
        parser = new StudentCsvParserImpl();
        this.fileName = "sample-data.csv";
        this.studentDTO = parser.parseCsv(fileName);
    }

    @Test
    public void testParseCsv() {
        try {
            parser.printCsv(this.studentDTO);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void shouldMatchTwoFile() throws IOException {

        //Get header real data
        Iterable<String> iterableHeader = parser.getHeaders(fileName);

        //Write in test CSV file
        try (CSVPrinter printer = new CSVPrinter(new FileWriter("test.csv"), CSVFormat.EXCEL)) {
            printer.printRecord(iterableHeader);
            for (StudentDTO dto : this.studentDTO) {
                printer.printRecord(dto.getStudentId(),dto.getFirstName(), dto.getLastName(), dto.getMiddleName());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        //Read and parse test CSV file
        Reader reader = Files.newBufferedReader(Paths.get("test.csv"));
        CSVParser parserTest = new CSVParser(reader, CSVFormat.DEFAULT.withHeader());

        //Get header test
        Map<String, Integer> header = parserTest.getHeaderMap();
        Iterable <String> iterableHeaderTest = new ArrayList<>(header.keySet());

        //Compare two headers
        assertTrue(iterableHeader.equals(iterableHeaderTest));

        //Parse test CSV
        List<StudentDTO> studentListTest = new ArrayList<>();
        for (CSVRecord record : parserTest) {
            StudentDTO dto = new StudentDTO();
            dto.setStudentId(Integer.parseInt(record.get("STUDENT_ID")));
            dto.setFirstName(record.get("FIRST_NAME"));
            dto.setLastName(record.get("LAST_NAME"));
            dto.setMiddleName(record.get("MIDDLE_NAME"));
            studentListTest.add(dto);
        }

        int n = 0;
        for(StudentDTO studentDTOReal : this.studentDTO){
            assertThat(studentDTOReal.getStudentId(),is(this.studentDTO.get(n).getStudentId()));
            assertThat(studentDTOReal.getFirstName(),is(this.studentDTO.get(n).getFirstName()));
            assertThat(studentDTOReal.getLastName(),is(this.studentDTO.get(n).getLastName()));
            assertThat(studentDTOReal.getMiddleName(),is(this.studentDTO.get(n).getMiddleName()));
            n++;
        }

    }

    @Test
    public void shouldParseFileToStream() {
        StudentDTO firstElement = this.studentDTO.get(0);
        StudentDTO elementTest = new StudentDTO();
        elementTest.setStudentId(1);
        elementTest.setFirstName("Michael");
        elementTest.setLastName("Fox");
        elementTest.setMiddleName("J");

        assertEquals(Arrays.asList(firstElement.getStudentId(), firstElement.getFirstName(), firstElement.getLastName(),
                firstElement.getMiddleName()),
                Arrays.asList(elementTest.getStudentId(), elementTest.getFirstName(), elementTest.getLastName(),
                        elementTest.getMiddleName()));
    }

    @Test
    public void shouldThrowExceptionWhenFileNotFound() throws IOException {
        thrown.expect(NullPointerException.class);
        parser.parseCsv( "filenotfound.txt");
    }

    @Test
    public void shouldThrowExceptionEmptyFileName() throws IOException {
        thrown.expect(NullPointerException.class);
        parser.parseCsv(" ");
    }

    @Test
    public void shouldAllColumnNotEmpty() {
        int emptyStudentId = 0;
        int emptyMiddleName = 0;
        int emptyFirstName = 0;
        int emptyLastName = 0;

        for (StudentDTO studentDTO : this.studentDTO) {
            if (studentDTO.getMiddleName().isEmpty())
                emptyMiddleName++;
            if (studentDTO.getFirstName().isEmpty())
                emptyMiddleName++;

            if (studentDTO.getLastName().isEmpty())
                emptyMiddleName++;
            if(studentDTO.getStudentId().toString().isEmpty())
                emptyStudentId++;

        }

        assertThat(emptyStudentId,is(0));
        assertThat(emptyFirstName,is(0));
        assertThat(emptyLastName,is(0));
        assertThat(emptyMiddleName,is(0));
    }
}