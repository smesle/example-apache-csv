package org.steve;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface StudentCsvParser {

    List<StudentDTO> parseCsv(String fileName) throws IOException;
    void printCsv(List<StudentDTO> StudentDTO) throws IOException;
    InputStream readRessource(String fileName);
    Iterable<String> getHeaders(String fileName) throws IOException;

}
