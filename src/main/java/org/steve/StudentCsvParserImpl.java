package org.steve;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.CSVParser;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StudentCsvParserImpl implements StudentCsvParser {

    public List<StudentDTO> parseCsv(String fileName) throws IOException {
        CSVParser parser = new CSVParser(new InputStreamReader(readRessource(fileName)), CSVFormat.EXCEL.withHeader());
        List<StudentDTO> studentList = new ArrayList<>();

        for (CSVRecord record : parser) {
            StudentDTO dto = new StudentDTO();

            dto.setStudentId(Integer.parseInt(record.get("STUDENT_ID")));
            dto.setFirstName(record.get("FIRST_NAME"));
            dto.setLastName(record.get("LAST_NAME"));
            dto.setMiddleName(record.get("MIDDLE_NAME"));
            studentList.add(dto);
        }

        return studentList;
    }

    public void printCsv(List<StudentDTO> StudentDTO) {
        for (StudentDTO dto : StudentDTO) {
            System.out.println("**********");
            System.out.println("USER_ID:" + dto.getStudentId());
            System.out.println("LAST_NAME:" + dto.getLastName());
            System.out.println("MIDDLE_NAME:" + dto.getMiddleName());
            System.out.println("FIRST_NAME:" + dto.getFirstName());
        }
    }

    public InputStream readRessource(String fileName) {
        InputStream inputStream = this.getClass().getClassLoader()
                .getResourceAsStream(fileName);
        return inputStream;
    }

    public Iterable<String> getHeaders(String fileName) throws IOException {
        CSVParser parser = new CSVParser(new InputStreamReader(readRessource(fileName)), CSVFormat.EXCEL.withHeader());
        Map<String, Integer> header = parser.getHeaderMap();
        Iterable <String> iterableStringHeader = new ArrayList<>(header.keySet());
        return iterableStringHeader;

    }

}